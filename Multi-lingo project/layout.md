## Goals:
- Simple layout per project
- easy to maintain
- good clean reusable code
- everything is handwritten (nothing forked from interwebs)

## Tech used:
- Typescript
- Python
- Golang
- Rust?
- GraphQL
- Kafka
- Dapr

## Typescript frontend
- Simple, nothing fancy but hooks into dapr

TS at the front, simple api calls to other backends to give a result

### Mark 1:
- TS frontend runs locally with single button "it works"
- Simple python backend returns weather of NYC

### Mark 2:
- Refine frontend to be usable
- Add TS testing
- Add Py Testing
- Python backend search based on input (validated)
- Py backend using GraphQL

### Mark 3:
- pub sub via kafka
- Argo events to decouple kafka and futureproof serverless
- Add golang option is TS frontend
- Add Go project with similar functionality to python (with testing)

### Mark 4:
- Add rust option
- k8 deployment with all applicaions


### Random notes
- Frontend = Constant
- Kafka / events = constant
- Backend code = knative 

- frontend -> queues a topic (via dapr)
- events listens to queues and triggers knative for lingo of choice
- lingo will pull state,secret etc from dapr and push update to a diff queue
- frontend will consume this queue

### Links
- (Distributed Calculator)[https://github.com/dapr/quickstarts/tree/master/distributed-calculator]

